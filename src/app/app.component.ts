import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MATERIAL_CONFIG } from './material.config';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    materialObj = {
        mat: [{
            matName: 'Test 1',
            matType: 'FERT',
            matBaseUnit: 'EA',
            length: 10.5,
            altUOM: 'EA'
        }],
        matUnit: [
            {
                altUOM: 'EA',
                length: 10.5,
                width: 10.5,
                height: 10.5
            },
            {
                altUOM: 'CASE',
                length: 30.5,
                width: 30.5,
                height: 20.5
            }
        ]
    };

    masterData = {
        matTypes: ['FERT', 'HALB', 'RAW MATERIAL'],
        workingCal: []
    };

    materialConfig = MATERIAL_CONFIG;

    // pageType (0=view,1=edit,2=create)
    pageType = 1;
    mainForm: FormGroup;
    mainFormGroups = {};

    constructor(
        private translate: TranslateService,
        private formBuilder: FormBuilder
    ) {
        this.translate.setDefaultLang('en');
        this.translate.use('en');
        this.createForm();
    }

    submit() {
        console.log(this.mainForm.value);
    }

    createForm() {
        this.materialConfig.tabs.forEach(tab => {
            this.mainFormGroups[tab.tblName] = this.formBuilder.array([]);
            if (tab.tabView === 'SECTION') {
                this.mainFormGroups[tab.tblName].push(this.formBuilder.group({}));
            }
        });
        this.mainForm = this.formBuilder.group(this.mainFormGroups);
    }
}
