import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class ComponentService {

    constructor(
        private fb: FormBuilder
    ) { }

    createFormFields(fieldsArray, masterData, groupObj, fieldValue = {}) {
        // const formGroup = new FormGroup({});
        fieldsArray.forEach(fieldsRow => {
            fieldsRow.forEach(field => {
                field.value = fieldValue[field.valueKey] || '';
                if (field.type === 'button' || field.type === 'blank') {
                    return;
                }
                if (field.type === 'select') {
                    field.options = masterData[field.option];
                }
                const control = this.fb.control(
                    field.value,
                    this.bindValidations(field.validations || [])
                );
                // mainFromGroupObj.get(tblName)['controls'][index].addControl(field.name, control);
                groupObj.addControl(field.name, control);
            });
        });
        // return mainFromGroupObj.get(tblName)['controls'][index];
        return groupObj;
    }

    bindValidations(validations: any) {
        if (validations.length > 0) {
            const validList = [];
            validations.forEach(valid => {
                validList.push(valid.validator);
            });
            return Validators.compose(validList);
        }
        return null;
    }
}
