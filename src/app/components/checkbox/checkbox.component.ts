
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../field.interface';

@Component({
    selector: 'app-checkbox',
    template: `
        <div class='demo-full-width form-group mb-1rem' [formGroup]='group' >
            <mat-checkbox [formControlName]='field.name'>{{field.label | translate}}</mat-checkbox>
        </div>`,
    styles: []
})

export class CheckboxComponent implements OnInit {
    field: FieldConfig;
    group: FormGroup;
    constructor() { }
    ngOnInit() { }
}
