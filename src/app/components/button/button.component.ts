

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../field.interface';
@Component({
    selector: 'app-button',
    template: `
        <div class='demo-full-width form-group mb-1rem' [formGroup]='group'>
            <button type='submit' mat-raised-button color='primary'>{{field.label | translate}}</button>
        </div>`,
    styles: []
})
export class ButtonComponent implements OnInit {
    field: FieldConfig;
    group: FormGroup;
    constructor() { }
    ngOnInit() { }
}
