import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { ComponentService } from '../component.service';

@Component({
	selector: 'app-dynamic-table',
	templateUrl: './dynamic-table.component.html',
	styles: [
	],
})
export class DynamicTableComponent implements OnInit {

	@Input() section;
	@Input() tableRecords;
	@Input() pageType;
	@Input() masterData;
	@Input() tblName;
	@Input() mainFromGroupObj: FormGroup;
	showForm = false;

	fromGroupObj: FormGroup;

	constructor(
		private fb: FormBuilder,
		private componentService: ComponentService
	) { }

	ngOnInit(): void {
		this.createTableForm();
	}

	createTableForm() {
		this.tableRecords.forEach((tableRecord) => {
			const rowFormGroup = this.componentService.createFormFields(
				this.section.fields,
				this.masterData,
				this.fb.group({}),
				tableRecord
			);
			(this.mainFromGroupObj.get(this.tblName) as FormArray).push(rowFormGroup);
		});
	}

	addNew() {
		this.showForm = true;
		this.fromGroupObj = this.componentService.createFormFields(
			this.section.fields,
			this.masterData,
			this.fb.group({}),
		);
	}

	editRecord(rowIndex) {
		this.showForm = true;
		this.fromGroupObj = this.mainFromGroupObj.get(this.tblName)['controls'][rowIndex];
	}

	submit(recordObj) {
		const recordIndex = this.mainFromGroupObj.get(this.tblName)['controls'].indexOf(recordObj);
		const recordValue = recordObj.value;
		if (recordIndex > -1) {
			console.log('edit');
			this.tableRecords[recordIndex] = recordValue;
		} else {
			this.tableRecords.push(recordValue);
			(this.mainFromGroupObj.get(this.tblName) as FormArray).push(this.fromGroupObj);
		}
		this.showForm = false;
	}


}
