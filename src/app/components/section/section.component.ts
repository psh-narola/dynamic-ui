import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-section',
    templateUrl: './section.component.html',
    styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {
    @Input() tabConfig;
    @Input() tabData;
    @Input() masterData;
    @Input() pageType;
    @Input() mainFromGroupObj;
    fromGroupObj: FormGroup;

    constructor() {
    }

    ngOnInit() {
        if(this.tabConfig.tabView === 'SECTION'){
            this.fromGroupObj = this.mainFromGroupObj.get(this.tabConfig.tblName)['controls'][0];
        }
    }
}
