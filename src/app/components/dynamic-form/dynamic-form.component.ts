import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output
} from '@angular/core';
import {
    FormGroup
} from '@angular/forms';
import { ComponentService } from '../component.service';

@Component({
    exportAs: 'dynamicForm',
    // tslint:disable-next-line:component-selector
    selector: 'dynamic-form',
    templateUrl: './dynamic-form.component.html',
    styles: []
})
export class DynamicFormComponent implements OnInit {
    @Input() fields = [];
    @Input() fieldValue?= [];
    @Input() masterData = {};
    @Input() pageType = {};
    @Input() tblName;
    @Input() fromGroupObj: FormGroup;
    // tslint:disable-next-line:no-output-native
    @Output() submit?: EventEmitter<any> = new EventEmitter<any>();

    form: FormGroup;

    constructor(
        private componentService: ComponentService
    ) { }

    ngOnInit() {
        // this.form = this.createControl(this.fieldValue[0], this.masterData);
        this.form = this.componentService.createFormFields(
            this.fields,
            this.masterData,
            this.fromGroupObj,
            this.fieldValue[0] || {}
        );
    }

    onSubmit(event: Event) {
        event.preventDefault();
        event.stopPropagation();
        if (this.form.valid) {
            this.submit.emit(this.form);
        } else {
            this.validateAllFormFields(this.form);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            control.markAsTouched({ onlySelf: true });
        });
    }

    /*
    createControl(fieldValue, masterData) {
        const group = this.fb.group({});
        this.fields.forEach(fieldsRow => {
            fieldsRow.forEach(field => {
                field.value = fieldValue[field.valueKey];
                if (field.type === 'button' || field.type === 'blank') {
                    return;
                }
                if (field.type === 'select') {
                    field.options = masterData[field.option];
                }
                const control = this.fb.control(
                    field.value,
                    this.bindValidations(field.validations || [])
                );
                this.mainFromGroupObj.get(this.tblName)['controls'][0].addControl(field.name, control);
            });
        });
        return this.mainFromGroupObj.get(this.tblName)['controls'][0];
    }

    bindValidations(validations: any) {
        if (validations.length > 0) {
            const validList = [];
            validations.forEach(valid => {
                validList.push(valid.validator);
            });
            return Validators.compose(validList);
        }
        return null;
    }
    */
}
