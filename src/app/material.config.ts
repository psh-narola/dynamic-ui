import { Validators } from '@angular/forms';

export const MATERIAL_CONFIG = {
    moduleLabel: 'MATERIAL.TITLE',
    moduleDescription: 'MATERIAL.DESCRIPTION',
    tabs: [
        {
            tabTitle: 'MATERIAL.DESCRIPTION_TAB.TITLE',
            tabDescription: 'MATERIAL.DESCRIPTION_TAB.DESCRIPTION',
            tabView: 'SECTION',
            tblName: 'mat',
            sections: [
                {
                    sectionTitle: 'MATERIAL.DESCRIPTION_TAB.SECTIONS.GENERAL_DATA',
                    fields: [
                        [
                            {
                                label: 'MATERIAL.FIELDS.MAT_NAME',
                                name: 'matName',
                                value: '',
                                valueKey: 'matName',
                                type: 'input',
                                inputType: 'text',
                                option: '',
                                isEditable: true,
                                validations: [
                                    { name: "required", validator: Validators.required, message: "MAT_NAME Required" }
                                ]
                            },
                            {
                                label: '',
                                name: '',
                                value: '',
                                valueKey: '',
                                type: 'blank',
                                inputType: '',
                                option: '',
                                isEditable: false,
                                validations: []
                            },
                            {
                                label: 'MATERIAL.FIELDS.MAT_TYPE',
                                name: 'matType',
                                value: '',
                                valueKey: 'matType',
                                type: 'select',
                                inputType: '',
                                option: 'matTypes',
                                isEditable: true,
                                validations: []
                            }
                        ]
                    ]
                },
                {
                    sectionTitle: 'MATERIAL.DESCRIPTION_TAB.SECTIONS.DIMENSIONS',
                    fields: [
                        [
                            {
                                label: 'MATERIAL.FIELDS.LENGTH',
                                name: 'length',
                                value: '',
                                valueKey: 'length',
                                type: 'input',
                                inputType: 'number',
                                option: '',
                                isEditable: true,
                                validations: [
                                    {
                                        name: 'pattern',
                                        validator: Validators.pattern('[0-9]+(\.[0-9][0-9]?)?'),
                                        message: 'Only 2 decimal points'
                                    }
                                ]
                            },
                            {
                                label: 'MATERIAL.FIELDS.ALT_UOM',
                                name: 'altUOM',
                                value: '',
                                valueKey: 'altUOM',
                                type: 'input',
                                inputType: 'text',
                                option: '',
                                isEditable: false,
                                validations: []
                            }
                        ],
                        [
                            {
                                label: 'MATERIAL.FIELDS.MAT_BASE_UNIT',
                                name: 'matBaseUnit',
                                value: '',
                                valueKey: 'matBaseUnit',
                                type: 'input',
                                inputType: 'text',
                                option: '',
                                isEditable: false,
                                validations: []
                            }
                        ]
                    ]
                }
            ]
        },
        {
            tabTitle: 'MATERIAL.UNIT_TAB.TITLE',
            tabDescription: 'MATERIAL.UNIT_TAB.DESCRIPTION',
            tabView: 'TABLE',
            tblName: 'matUnit',
            sections: [
                {
                    sectionTitle: 'MATERIAL.DESCRIPTION_TAB.SECTIONS.GENERAL_DATA',
                    fields: [
                        [
                            {
                                label: 'MATERIAL.FIELDS.ALT_UOM',
                                name: 'altUOM',
                                value: '',
                                valueKey: 'altUOM',
                                type: 'input',
                                inputType: 'text',
                                option: '',
                                isEditable: true,
                                validations: [
                                    { name: "required", validator: Validators.required, message: "Please enter ALT UOmo" }
                                ]
                            },
                            {
                                label: 'MATERIAL.FIELDS.LENGTH',
                                name: 'length',
                                value: '',
                                valueKey: 'length',
                                type: 'input',
                                inputType: 'number',
                                option: '',
                                isEditable: true,
                                validations: [
                                    { name: "required", validator: Validators.required, message: "Please enter length." },
                                    {
                                        name: 'pattern',
                                        validator: Validators.pattern('[0-9]+(\.[0-9][0-9]?)?'),
                                        message: 'Only 2 decimal points'
                                    }
                                ]
                            },
                            {
                                label: 'MATERIAL.FIELDS.WIDTH',
                                name: 'width',
                                value: '',
                                valueKey: 'width',
                                type: 'input',
                                inputType: 'number',
                                option: '',
                                isEditable: true,
                                validations: [
                                    { name: "required", validator: Validators.required, message: "Please enter Width." },
                                    {
                                        name: 'pattern',
                                        validator: Validators.pattern('[0-9]+(\.[0-9][0-9]?)?'),
                                        message: 'Only 2 decimal points'
                                    }
                                ]
                            },
                            {
                                label: 'MATERIAL.FIELDS.HEIGHT',
                                name: 'height',
                                value: '',
                                valueKey: 'height',
                                type: 'input',
                                inputType: 'number',
                                option: '',
                                isEditable: true,
                                validations: [
                                    { name: "required", validator: Validators.required, message: "Please enter height." },
                                    {
                                        name: 'pattern',
                                        validator: Validators.pattern('[0-9]+(\.[0-9][0-9]?)?'),
                                        message: 'Only 2 decimal points'
                                    }
                                ]
                            }
                        ],
                        [
                            {
                                type: 'button',
                                label: 'Save',
                            }
                        ]
                    ]
                }
            ]
        }
    ]
};
